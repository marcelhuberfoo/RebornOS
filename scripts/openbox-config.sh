pacman -S openbox-theme --force --noconfirm
wget --spider www.google.com
if [ "$?" = 0 ]; then
echo "$(ls /home/)" >/home.txt
sed -i '/timeshift/d' /home.txt
sed -i '/root/d' /home.txt
if [[ $(wc -l /home.txt | awk '{print $1}') -ge 2 ]]; then
sed -i '/reborn/d' /home.txt
else
echo "Continuing on..."
fi
mkdir /home/$(cat /home.txt)/.config/gtk-3.0
cp -f /tmp/settings.ini /home/$(cat /home.txt)/.config/gtk-3.0/
cp -f /tmp/rc.xml /home/$(cat /home.txt)/.config/openbox/
mkdir /home/$(cat /home.txt)/.config/conky
cp -f /tmp/conky.conf /home/$(cat /home.txt)/.config/conky/
rm -f /tmp/conky.conf
chmod ugo=rw /home/$(cat /home.txt)/.config/conky/conky.conf
chmod ugo=rw /home/$(cat /home.txt)/.config/openbox/rc.xml
chmod ugo=rw /home/$(cat /home.txt)/.config/gtk-3.0/settings.ini
git clone https://github.com/addy-dclxvi/openbox-theme-collections.git /temp
rm -rf /temp/.git
mv -f /temp/* /usr/share/themes/
rm -rf /temp
git clone https://github.com/addy-dclxvi/gtk-theme-collections.git /temp2
rm -rf /temp2/.git
mv -f /temp2/* /usr/share/themes/
rm -rf /temp2
mv -f /tmp/settings.ini /usr/share/gtk-3.0/
mv -f /tmp/rc.xml /etc/xdg/openbox/
rm -f /etc/xdg/autostart/obmenu-gen.desktop
sudo -u $(cat /home.txt) obmenu-generator -p -i
sudo -u $(cat /home.txt) openbox --reconfigure
yad --title "Conky" --height=100 --width=300 --center --text="Your Openbox setup has now been fully configured! To experience the improvements fully, please log out and then back in. Thank you, and enjoy!" --text-align=center
sudo -u root rm -f /home.txt
else exec /usr/bin/openbox-config.sh
fi
